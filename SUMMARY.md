# Table of contents

* [CHANGELOG](README.md)
* [Contributing to XML Tools](contributing.md)
* [XML Tools for Visual Studio Code](readme.md)
* [.github](.github/README.md)
  * [ISSUE\_TEMPLATE](.github/issue\_template/README.md)
    * [bug-report](.github/issue\_template/bug-report.md)
  * [ISSUE\_TEMPLATE](.github/issue\_template-1/README.md)
    * [feature-request](.github/issue\_template-1/feature-request.md)
  * [ISSUE\_TEMPLATE](.github/issue\_template-2/README.md)
    * [xml-formatter](.github/issue\_template-2/xml-formatter.md)
